#!/bin/sh
echo "-------- Peparing examples ------------"
res/prepareExamples.sh
git status
echo "---------------------------------------"
echo "----------- Commiting -----------------"
git commit -am "$*"
echo "---------------------------------------"
echo "------------- Sending -----------------"
git push
echo "-------------- Done -------------------"