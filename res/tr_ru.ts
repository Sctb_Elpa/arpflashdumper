<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>ARP</name>
    <message>
        <location filename="../src/ARP.cpp" line="59"/>
        <source>Error writing address selector value.</source>
        <translation>Ошибка записа адреса сектора.</translation>
    </message>
    <message>
        <location filename="../src/ARP.cpp" line="100"/>
        <source>Error %1 occurred while reading, rereading fragment</source>
        <translation>Ошибка %1 во время чтения. Перечитываню фрагмент.</translation>
    </message>
    <message>
        <location filename="../src/ARP.cpp" line="121"/>
        <source>Error %1 occurred while reading, fragment is now filled &apos;0xff&apos;</source>
        <translation>Ошибка чтения %1, фрагмент заполняется &apos;0xff&apos; </translation>
    </message>
    <message>
        <location filename="../src/ARP.cpp" line="133"/>
        <source>General modbus error occurred while reading, block is now filled &apos;0xff&apos;</source>
        <translation>Общая ошибка modbas %1, блок заполняется &apos;0xff&apos;</translation>
    </message>
    <message>
        <location filename="../src/ARP.cpp" line="163"/>
        <source>Failed to read serial number from device at address %1.</source>
        <translation>Неудачная попытка чтения серийного номера устройства по адресу %1.</translation>
    </message>
    <message>
        <location filename="../src/ARP.cpp" line="188"/>
        <source>Found unsupported device ID: %1 at address %2</source>
        <translation>Обнаружено неподдерживаемое устройство ID: %1 по адресу %2</translation>
    </message>
    <message>
        <location filename="../src/ARP.cpp" line="213"/>
        <source>Device at address %1 memory used: %2 bytes.</source>
        <translation>Использование памяти устройством по адресу %1: %2 Байт.</translation>
    </message>
    <message>
        <location filename="../src/ARP.cpp" line="219"/>
        <source>Failed to read memory counter from device at address %1.</source>
        <translation>Очибка чтения значения счетчика памяти устройства по адресу %1.</translation>
    </message>
</context>
<context>
    <name>ARPReader</name>
    <message>
        <location filename="../src/ARPReader.cpp" line="55"/>
        <source>Start reading device %1, Serial: %2, memory used: %3 Bytes</source>
        <translation>Начало чтения с устройства %1, Серийный номер %2, памяти занято %3 Байт.</translation>
    </message>
    <message>
        <location filename="../src/ARPReader.cpp" line="66"/>
        <source>Output file: %1</source>
        <translation>Выходнйо файл: %1</translation>
    </message>
    <message>
        <location filename="../src/ARPReader.cpp" line="70"/>
        <source>Failed to open file! Exiting.</source>
        <translation>Не удалось открыть файл! Выход.</translation>
    </message>
    <message>
        <location filename="../src/ARPReader.cpp" line="82"/>
        <source>&apos;From to Error&apos; (%1 &gt; %2). Break.</source>
        <translation>Ошибка &apos;от - до&apos; (%1 &gt; %2). Остановка.</translation>
    </message>
    <message>
        <location filename="../src/ARPReader.cpp" line="86"/>
        <source>Start block: %1	End block: %2</source>
        <translation>Начальный блок: %1\tКонечный блок: %2</translation>
    </message>
    <message>
        <location filename="../src/ARPReader.cpp" line="99"/>
        <source>Error while reading block %1.</source>
        <translation>Ошибка чтения блока %1.</translation>
    </message>
    <message>
        <location filename="../src/ARPReader.cpp" line="103"/>
        <source>Error While processing device %1!, break. 
 (Note: specify &apos;--force&apos; flag to ignore this error).</source>
        <translation>Ошибка при работе с устройством %1!, останов.\n (Замечание: Попробуйте ключ &apos;--force&apos; для игнорирования этой ошибки).</translation>
    </message>
    <message>
        <location filename="../src/ARPReader.cpp" line="118"/>
        <source>Error While processing device info!</source>
        <translation>Ошибка обработки информации об устройстве!</translation>
    </message>
    <message>
        <location filename="../src/ARPReader.cpp" line="121"/>
        <source>Complied!</source>
        <translation>Готово!</translation>
    </message>
</context>
<context>
    <name>Log</name>
    <message>
        <location filename="../src/Log.cpp" line="27"/>
        <source>Log started.</source>
        <translation>Журнал начат.</translation>
    </message>
    <message>
        <location filename="../src/Log.cpp" line="47"/>
        <location filename="../src/Log.cpp" line="62"/>
        <source>[DEBUG]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Log.cpp" line="50"/>
        <source>[INFO]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Log.cpp" line="53"/>
        <source>[WARNING]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Log.cpp" line="56"/>
        <source>[ERROR]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Log.cpp" line="59"/>
        <source>[FATAL]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Log.cpp" line="66"/>
        <source>[%1]	</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Log.cpp" line="73"/>
        <source>[%1][%2]</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ModbusEngine</name>
    <message>
        <location filename="../src/ModbusEngine.cpp" line="273"/>
        <source>Modbus request to %1 failed, errorcode=%2 (req. type=%3)</source>
        <translation>Запрос modbus к устройству %1 неудачен, код ошибки = %2 (тип запроса = %3)</translation>
    </message>
    <message>
        <location filename="../src/ModbusEngine.cpp" line="283"/>
        <source>Modbus connect error detected (%1)</source>
        <translation>Обнаружена ошибка соединения modbus (%1)</translation>
    </message>
    <message>
        <location filename="../src/ModbusEngine.cpp" line="385"/>
        <source>ModbusEngine::RestartConnection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ModbusEngine.cpp" line="390"/>
        <source>Opening serial device &quot;%1&quot;</source>
        <translation>Открываем последовательный порт \&quot;%1\&quot;</translation>
    </message>
    <message>
        <location filename="../src/ModbusEngine.cpp" line="398"/>
        <source>Opening failed</source>
        <translation>Ощибка открытия</translation>
    </message>
    <message>
        <location filename="../src/ModbusEngine.cpp" line="416"/>
        <source>%1 autoupdate cycle</source>
        <translation>%1 цикл автообновления.</translation>
    </message>
    <message>
        <location filename="../src/ModbusEngine.cpp" line="416"/>
        <source>Enabling</source>
        <translation>Разрешаем</translation>
    </message>
    <message>
        <location filename="../src/ModbusEngine.cpp" line="416"/>
        <source>Disabling</source>
        <translation>Запрещаем</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Usage: ARPFlashDumper [-h | --help] [options]
	    
Options:
	    	 -p	&lt;port&gt;		 Serial Device. (Default: %1 )
	    	 -b	&lt;Baud&gt;		 Serial Device Baud. (Default: %2)
	    	 -d	&lt;DeviceAddress&gt;	 Modbus Device Address. (Default: First found.)
	    	 -o	&lt;outputFileName&gt; Dump File Name. (Default: %3)

	    	 --auto			 Full automatic mode. Read all found devices, write to files with names like: %4
	    	 --from=&lt;start_pocket&gt;	 Specify Start Pocket Number. (Default: %5)
	    	 --to=&lt;end_pocket&gt;	 Specify End Pocket Number. (Default: By memory counter)
	    	 --log-level=&lt;log_level&gt; Specifay Log Verbose Level: [Error|Debug|Info|Warning] (Default: %6)
	    	 --force		 Ignore read errors.
	    	 --read-at-once=&lt;size&gt;	 Modbus size at once max cells. (Default: %7)
	    </source>
        <translation type="obsolete">Использование: ARPFlashDumper [-h | --help] [параметры]
	    
Options:
	    	 -p	&lt;COM-порт&gt;		 Используемый COM-порт. (Default: %1 )
	    	 -b	&lt;скорость&gt;		 Скорость порта. (Default: %2)
	    	 -d	&lt;Адрес&gt;	 Адрес на шине Modbus. (Default: первый найденый.)
	    	 -o	&lt;ИмяВыходногоФайла&gt; Dump File Name. (Default: %3)

	    	 --auto			 Полная автоматизация. Читает все найденые устройства, записывает данные в файлы с именами вида: %4
	    	 --from=&lt;НачальныйПакет&gt;	 Начать чтоение с блока... (Default: %5)
	    	 --to=&lt;КонечныйПакет&gt;	 Читать до блока... (Default: до конца записи)
	    	 --log-level=&lt;УровеньЛога&gt; 	 Количество отображаемой информации: [Error|Debug|Info|Warning] (Default: %6)
	    	 --force		 Игнорировать ошибки чтения.
	    	 --read-at-once=&lt;размер&gt;	 Читать по ... регистров за раз (ТОЛЬКО ДЛЯ ЭКСПЕРТА). (Default: %7)
      </translation>
    </message>
    <message>
        <source>Usage: ARPFlashDumper [-h | --help] [options]
	    
Options:
	    	 -p	&lt;port&gt;		 Serial Device. (Default: %1 )
	    	 -b	&lt;Baud&gt;		 Serial Device Baud. (Default: %2)
	    	 -d	&lt;DeviceAddress&gt;	 Modbus Device Address. (Default: First found.)
	    	 -o	&lt;outputFileName&gt; Dump File Name. (Default: %3)

	    	 --auto			 Full automatic mode. Read all found devices, write to files with names like: %4
	    	 --from=&lt;start_pocket&gt;	 Specify Start Pocket Number. (Default: %5)
	    	 --to=&lt;end_pocket&gt;	 Specify End Pocket Number. (Default: By memory counter)
	    	 --log-level=&lt;log_level&gt;	 Specifay Log Verbose Level: [Error|Debug|Info|Warning] (Default: %6)
	    	 --force		 Ignore read errors.
	    	 --read-at-once=&lt;size&gt;	 Modbus size at once max cells. (Default: %7)
	    </source>
        <translation type="obsolete">Использование: ARPFlashDumper [-h | --help] [параметры]
	    
Options:
	    	 -p	&lt;COM-порт&gt;		 Используемый COM-порт. (Default: %1 )
	    	 -b	&lt;скорость&gt;		 Скорость порта. (Default: %2)
	    	 -d	&lt;Адрес&gt;	 Адрес на шине Modbus. (Default: первый найденый.)
	    	 -o	&lt;ИмяВыходногоФайла&gt; Dump File Name. (Default: %3)

	    	 --auto			 Полная автоматизация. Читает все найденые устройства, записывает данные в файлы с именами вида: %4
	    	 --from=&lt;НачальныйПакет&gt;	 Начать чтоение с блока... (Default: %5)
	    	 --to=&lt;КонечныйПакет&gt;	 Читать до блока... (Default: до конца записи)
	    	 --log-level=&lt;УровеньЛога&gt; 	 Количество отображаемой информации: [Error|Debug|Info|Warning] (Default: %6)
	    	 --force		 Игнорировать ошибки чтения.
	    	 --read-at-once=&lt;размер&gt;	 Читать по ... регистров за раз (ТОЛЬКО ДЛЯ ЭКСПЕРТА). (Default: %7)
      </translation>
    </message>
    <message>
        <location filename="../src/CmdLineAnalyser.cpp" line="115"/>
        <source>Usage: ARPFlashDumper [-h | --help] [options]
	    
Options:
	    	 -p	&lt;port&gt;		 Serial Device. (Default: %1 )
	    	 -b	&lt;Baud&gt;		 Serial Device Baud. (Default: %2)
	    	 -d	&lt;DeviceAddress&gt;	 Modbus Device Address. (Default: First found.)
	    	 -o	&lt;outputFileName&gt; Dump File Name. (Default: %3)

	    	 --auto		 Full automatic mode. Read all found devices, write to files with names like: %4
	    	 --from=&lt;start_pocket&gt;	 Specify Start Pocket Number. (Default: %5)
	    	 --to=&lt;end_pocket&gt;	 Specify End Pocket Number. (Default: By memory counter)
	    	 --log-level=&lt;log_level&gt;	 Specifay Log Verbose Level: [Error|Debug|Info|Warning] (Default: %6)
	    	 --force		 Ignore read errors.
	    	 --read-at-once=&lt;size&gt;	 Modbus size at once max cells. (Default: %7)
	    </source>
        <translation>Использование: ARPFlashDumper [-h | --help] [параметры]
	    
Options:
	    	 -p	&lt;COM-порт&gt;		 Используемый COM-порт. (Default: %1 )
	    	 -b	&lt;скорость&gt;		 Скорость порта. (Default: %2)
	    	 -d	&lt;Адрес&gt;		 Адрес на шине Modbus. (Default: первый найденый.)
	    	 -o	&lt;ИмяВыходногоФайла&gt;	 Ямя файла-дампа. (Default: %3)

	    	 --auto		 Полная автоматизация. Читает все найденые устройства, записывает данные в файлы с именами вида: %4
	    	 --from=&lt;НачальныйПакет&gt;	 Начать чтоение с блока... (Default: %5)
	    	 --to=&lt;КонечныйПакет&gt;	 Читать до блока... (Default: до конца записи)
	    	 --log-level=&lt;УровеньЛога&gt; 	 Количество отображаемой информации: [Error|Debug|Info|Warning] (Default: %6)
	    	 --force		 Игнорировать ошибки чтения.
	    	 --read-at-once=&lt;размер&gt;	 Читать по ... регистров за раз (ТОЛЬКО ДЛЯ ЭКСПЕРТА). (Default: %7)
      </translation>
    </message>
    <message>
        <location filename="../src/CmdLineAnalyser.cpp" line="226"/>
        <source>Debug</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/CmdLineAnalyser.cpp" line="228"/>
        <source>Info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/CmdLineAnalyser.cpp" line="230"/>
        <source>Warning</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/CmdLineAnalyser.cpp" line="232"/>
        <source>Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ModbusEngine.cpp" line="292"/>
        <source>Autoupdate interval set to %1</source>
        <translation>Интервал автообновления установлен на %1</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="79"/>
        <location filename="../src/main.cpp" line="98"/>
        <location filename="../src/main.cpp" line="112"/>
        <source>ARP device detected at address %1, serial number: %2</source>
        <translation>Обнаружен АРП по адресу %1, серийный номер: %2 </translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="85"/>
        <source>Detecting failed!</source>
        <translation>Не найдено ни одного устройства!</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="91"/>
        <source>Detecting devices, please wait.</source>
        <translation>Поиск устройств, пожалуйста подождите.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="103"/>
        <source>Detection finished: %1.</source>
        <translation>Поиск завершен: %1.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="103"/>
        <source>Fail</source>
        <translation>пусто</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="103"/>
        <source>Success</source>
        <translation>удачно</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="118"/>
        <source>Specified ARP device not detected at address %1. Exit.</source>
        <translation>Устройство не было обнаружено по указанному адресу (%1). Выход.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="128"/>
        <source>No ARP devices found! Exiting.</source>
        <translation>АРП не обнаружено! Завершение.</translation>
    </message>
</context>
</TS>
