TEMPLATE = app
TARGET = ARPFlashDumper
CONFIG += debug_and_release \
    console \
    profiling

# CONFIG += infinty_reread_try
# CONFIG += use_translation
# CONFIG += modbus_debug
# CONFIG += nodevises
QT += core
QT -= gui
use_translation:DEFINES += _USE_TRANSLATION
nodevises:DEFINES += _NODEVICES
modbus_debug:DEFINES += __MODBUS_DEBUG_MODE=1
profiling { 
    QMAKE_CXXFLAGS_DEBUG += -pg
    QMAKE_CFLAGS_DEBUG += -pg
    QMAKE_LFLAGS_DEBUG += -pg
}
infinty_reread_try:DEFINES += INF_REREAD_TRY
HEADERS += include/QDateTimeDelta.h \
    include/ARPReader.h \
    include/ARP.h \
    include/CmdLineAnalyser.h \
    include/SimpleOpt.h \
    include/CleanExit.h
INCLUDEPATH += include
SOURCES += src/QDateTimeDelta.cpp \
    src/ARPReader.cpp \
    src/ARP.cpp \
    src/CmdLineAnalyser.cpp \
    src/main.cpp \
    src/CleanExit.cpp
TRANSLATIONS = res/tr_ru.ts
REVISION_INPUT = res/Revision_template.cpp
revtarget.input = REVISION_INPUT
revtarget.output = include/Revision.h
revtarget.commands = sh \
    res/make_revision.sh \
    ${QMAKE_FILE_NAME} \
    ${QMAKE_FILE_OUT}
revtarget.depends = ${QMAKE_FILE_NAME} \
    $$SOURCES \
    $$HEADERS
revtarget.name = Generating_RevNum \
    (${QMAKE_FILE_NAME})
revtarget.variable_out = HEADERS
QMAKE_EXTRA_COMPILERS += revtarget
unix { 
    LIBS += -lmodbus \
        -lqjson \
        -lmylog \
        -lqmodbus \
        -lsettingscmdline \
        -L/usr/local/lib
    CONFIG(release, debug|release):DESTDIR = release
    CONFIG(debug, debug|release) { 
        DEFINES += _GLIBCXX_DEBUG
        DESTDIR = debug
    }
    INCLUDEPATH += /usr/local/include \
        /usr/local/include/modbus \
        /usr/local/include/settingscmdline \
        /usr/local/include/mylog \
        /usr/local/include/qmodbus
    OBJECTS_DIR = $$(DESTDIR)
    MOC_DIR = $$(DESTDIR)
}
win32 { 
    LIBS += -lmodbus \
        -lqjson \
        -lmylog \
        -lqmodbus \
        -lsettingscmdline \
        -L'$(P)/usr/local/lib'
    INCLUDEPATH += $(P)/usr/local/include \
        $(P)/usr/local/include/modbus \
        $(P)/usr/local/include/settingscmdline \
        $(P)/usr/local/include/mylog \
        $(P)/usr/local/include/qmodbus \
        c:/MinGW/include
}
