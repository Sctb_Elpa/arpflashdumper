/*
 * CmdLineAnalyser.h
 *
 *  Created on: 04.02.2013
 *      Author: tolyan
 */

#ifndef CMDLINEANALYSER_H_
#define CMDLINEANALYSER_H_

#include <QMap>
#include <QString>
#include <QVariant>

#include <settingscmdline/SimpleOpt.h>
#include <settingscmdline/settingscmdline.h>

#ifdef WIN32
#define _DEFAULT_PORT	"COM1"
#else
#define _DEFAULT_PORT	"/dev/ttyUSB0"
#endif
#define _DEFAULT_SPEED	(57600)

// читать регистрок за раз не более
#ifndef ARP_MAX_READ_ONCE
#define ARP_MAX_READ_ONCE		(55)
#endif

class CmdLineAnalyser: public SettingsCmdLine::cSettingsCmdLine
    {
public:
    enum enSaveOpts
	{
	SaveOpts_TestOnly = 1 << 0,
	SaveOpts_Coeffs_save = 1 << 1,
	SaveOpts_Data_save = 1 << 2,
	SaveOpts_Clear_device = 1 << 3
	};

    enum DeviceAdress
	{
	DeviceAdress_First_Found = 257,
	DeviceAdress_All_Found = 258,
	};

    CmdLineAnalyser(int argc, char *argv[]);

    static struct SettingsCmdLine::key_val_res parse_Arg(int argCode,
	    const char* optText, char* ArgVal, cSettingsCmdLine* origins);
    static void SetDefaultValues(cSettingsCmdLine* _this);
    static void console_ShowUsage(cSettingsCmdLine* _this);

private:
    static SettingsCmdLine::pfTable table;
    };

#endif /* CMDLINEANALYSER_H_ */
