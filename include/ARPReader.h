/*
 * ARPReader.h
 *
 *  Created on: 05.02.2013
 *      Author: tolyan
 */

#ifndef ARPREADER_H_
#define ARPREADER_H_

#include <QObject>
#include <QList>
#include <QDateTime>
#include <QByteArray>

class ARP;

class ARPReader: public QObject
    {
Q_OBJECT
public:
    ARPReader(QList<ARP*>* _arpList, QObject* parent = NULL);
    virtual ~ARPReader();

public slots:
    void Exec();

private:
    QList<ARP*>* arpList;

    int offset;
    int endblock;
    int currentPos;
    unsigned char pos;
    static char m[];
    int outputFreqDev;

    QDateTime time;

    static QByteArray md5CorrectHash;

public slots:
    void ShowProgress(qreal pAdd = 0);
    };

#endif /* ARPREADER_H_ */
