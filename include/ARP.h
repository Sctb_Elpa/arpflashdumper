/*
 * ARP.h
 *
 *  Created on: 04.02.2013
 *      Author: tolyan
 */

#ifndef ARP_H_
#define ARP_H_

#include <stdint.h>

#include <QObject>
#include <QList>
#include <QByteArray>
#include <QVariantMap>
#include <QDateTime>

#include <qmodbus/ModbusEngine.h>

// идентификатор
#define ARP_DEVICE_ID			(0xDB05)

// размер блока (регистров)
#define ARP_BLOCK_SIZE			(8192)
// количество блоков
#define ARP_BLOAK_COUNT			(65535)

// адресс идентификатора (holding)
#define ARP_DEVICE_ID_ADDRESS		(0x0000)
// адресс адрес серийного номера (holding)
#define ARP_SERIAL_ADDRESS		(0x0001)
// адресс ячейки задающей адрес (holding)
#define ARP_BLOCK_SELECTOR_ADDRESS	(0x0040)
// адрес начала бдока занных (input)
#define ARP_DATA_START_ADDRESS		(0x0400)
// адрес начала бдока занных (input) (uint32_t)
#define ARP_MEMORY_COUNTER_ADDRESS	(0x0008)

// начало блока коэфициентов
#define ARP_COEFFS_START_ADRESS		(0x0020)
// количество коэфициентов
#define ARP_COEFFS_COUNT		(13)

// адрес даты (holding)
#define ARP_DATE_START_ADRESS         	(0x004A)

// адрес флага стирания
#define CLEAR_FLAG_ADDRES		(0x000E)

class ModbusRequest;

class ARP: public QObject
    {
    Q_OBJECT
public:
    union U_Date
	{
    struct
    {
	unsigned char sec;
	unsigned char min;
	unsigned char hour;
	unsigned char day;
	unsigned char month;
	unsigned char year;
    } _Date;
    char raw[6];
	};


    ARP(unsigned char mb_adress, ModbusEngine* _mb, QObject *parent = NULL);
    virtual ~ARP();

    bool test();
    uint16_t getSerial(bool* isOk = NULL);
    uint32_t getMemoryCounter(bool* isOk = NULL);

    QByteArray dataAtBlock(uint16_t block, bool *ok = NULL);
    unsigned char getadress() const;

    bool writeBlockAdress(uint16_t block);

    QVariantMap getCoeffs(bool *ok = NULL);

    static ARP* detector(unsigned char startAdress, ModbusEngine* mb);

    QDateTime getDate(bool *ok = NULL);

    bool Erease();

private:
    void buildreadRequests();

    unsigned char mbadress;
    QList<ModbusRequest*> readRequests;

    static unsigned int readAtOnce;
    ModbusEngine* mb;

    signals:
    void UpdateProgress(qreal precentAdd);
    };

#endif /* ARP_H_ */
