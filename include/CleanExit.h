#ifndef CLEANEXIT_H
#define CLEANEXIT_H

//из http://blog.alexei-developer.ru/?p=655

#include <QObject>

#include <QCoreApplication>
#include <QDebug>
#include <csignal>

class CleanExit: public QObject
    {
Q_OBJECT
public:
    explicit CleanExit(QObject *parent = NULL);

private:
    static void exitQt(int sig);
    };

#endif // CLEANEXIT_H
