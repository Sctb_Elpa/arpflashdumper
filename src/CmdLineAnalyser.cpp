/*
 * CmdLineAnalyser.cpp
 *
 *  Created on: 04.02.2013
 *      Author: tolyan
 */

#include <cstdio>
#include <iostream>

#include <QIODevice>

#include <mylog/mylog.h>

#include "CmdLineAnalyser.h"

// типы ключей
enum enArgs
    {
    OPT_HELP,
    OPT_PORT,
    OPT_BAUD,
    OPT_DEVADR,
    OPT_OUT,
    OPT_APPEND,
    OPT_AUTO,
    OPT_FROM,
    OPT_TO,
    OPT_LOG_LEVEL,
    OPT_NOFORCE,
    OPT_READ_AT_ONCE,
    OPT_COEFFS_ONLY,
    OPT_NO_COEFFS,
    OPT_TEST_ONLY,
    OPT_TEST_CLEAR,
    OPT_CLEAR_DEVICE
    };

// массив ключей программы
static CSimpleOpt::SOption g_rgOptions[] =
    {
	{
	OPT_HELP, _T("-h"), SO_NONE
	}, // "-h"
		{
		OPT_HELP, _T("--help"), SO_NONE
		}, // "--help"
		   //-----------------
		{
		OPT_PORT, _T("-p"), SO_REQ_SEP
		}, // "-p <PORT>"
		   //-----------------
		{
		OPT_BAUD, _T("-b"), SO_REQ_SEP
		}, // "-b <Baud>"
		   //-----------------
		{
		OPT_DEVADR, _T("-d"), SO_REQ_SEP
		}, // "-d <DeviceAddress>"
		   //-----------------
		{
		OPT_OUT, _T("-o"), SO_REQ_SEP
		}, // "-o <outputfilename>"
		   //-----------------
		{
		OPT_APPEND, _T("-a"), SO_NONE
		}, // "-a"
		   //-----------------
		{
		OPT_AUTO, _T("--auto"), SO_NONE
		}, // "--auto"
		   // автоматический режим, ищет на шине все устройства и читает все с них
		{
		OPT_NOFORCE, _T("--noforce"), SO_NONE
		}, // "--force"
		   // Игнорировать ошибки чтения
		{
		OPT_FROM, _T("--from"), SO_REQ_CMB
		}, // "--from=<начальный пакет>"
		   //-----------------
		{
		OPT_TO, _T("--to"), SO_REQ_CMB
		}, // "--to=<конечный пакет>"
		   //-----------------
		{
		OPT_LOG_LEVEL, _T("--log-level"), SO_REQ_CMB
		}, // "--log-level=<уровень лога>"
		   //-----------------
		{
		OPT_READ_AT_ONCE, _T("--read-at-once"), SO_REQ_CMB
		}, // "--log-level=<Читать ячеек modbus за раз>"
		   //-----------------
		{
		OPT_COEFFS_ONLY, _T("--coeffs-only"), SO_NONE
		}, // "--coeffs-only"
		   // прочитать только коэфициенты
		   //-----------------
		{
		OPT_NO_COEFFS, _T("--no-coeffs"), SO_NONE
		}, // "--no-coeffs"
		   // Не выгружать коэфициенты
		   //-----------------
		{
		OPT_TEST_ONLY, _T("-t"), SO_NONE
		}, // "-t"
		   // Только тест
		//-----------------
		{
		OPT_TEST_CLEAR, _T("--test-clear"), SO_NONE
		}, // "-t"
		    // Тест на стирание
		//-----------------
		{
		OPT_CLEAR_DEVICE, _T("--clear"), SO_NONE
		}, // "-clear"
		   // Стереть девайс
	    SO_END_OF_OPTIONS
    // END
	};

SettingsCmdLine::pfTable CmdLineAnalyser::table =
    {
	    g_rgOptions,
	    CmdLineAnalyser::SetDefaultValues,
	    CmdLineAnalyser::console_ShowUsage,
	    CmdLineAnalyser::parse_Arg
    };

void CmdLineAnalyser::console_ShowUsage(cSettingsCmdLine* _this)
    {
    std::cout
	    << QObject::trUtf8(
		    "Usage: ARPFlashDumper [-h | --help] [options]\n\
\n\
Options:\n\
 -p\t<port>\t\t Serial Device. (Default: %1 )\n\
 -b\t<Baud>\t\t Serial Device Baud. (Default: %2)\n\
 -d\t<DeviceAddress>\t Modbus Device Address. (Default: First found.)\n\
 -o\t<outputFileName> Dump File Name. (Default: %3)\n\n\
 --auto\t\t\t Full automatic mode. Read all found devices, write to files with names like: %4\n\
 --from=<start_pocket>\t Specify Start Pocket Number. (Default: %5)\n\
 --to=<end_pocket>\t Specify End Pocket Number. (Default: By memory counter)\n\
 --log-level=<log_level> Specify Log Verbose Level: [Error|Debug|Info|Warning] (Default: %6)\n\
 --noforce\t\t Stop on first error.\n\
 --read-at-once=<size>\t Modbus size at once max cells. (Default: %7)\n\
 -a\t\t\t Append output data to file (Default: disabled)\n\
 -t\t\t\t Test mode, read data and check MD5 sum with pattern.\n\
 --test-clear\t\t Change test pattern to (0xffff) * 8192\n\
 --coeffs-only\t\t Read coeffs from device and exit. (Default: off)\n\
 --no-coeffs\t\t Don't read coeffs from device. (Default: off)\n\
 --clear\t\t Erase Device. (Can be combine with option '-t')\n").arg(
		    (*_this)["Global/Port"].toString()).arg(
		    (*_this)["Global/Baud"].toString()).arg(
		    (*_this)["Output"].toString()).arg(
		    (*_this)["Output"].toString()).arg(
		    (*_this)["Start"].toString()).arg(
		    (*_this)["Log-level"].toString()).arg(
		    (*_this)["ReadAtOnce"].toString()).toLatin1().data();
    }

struct SettingsCmdLine::key_val_res CmdLineAnalyser::parse_Arg(int optCode,
	const char* optText, char *ArgVal, cSettingsCmdLine* origins)
    {
    SettingsCmdLine::key_val_res result =
	{
	QString(), QVariant(), true
	};
    switch (optCode)
	{
    case OPT_PORT:
	result.key = "Global/Port";
	result.val = ArgVal;
	break;
    case OPT_BAUD:
	result.key = "Global/Baud";
	result.val = QString(ArgVal).toUInt(&result.res);
	if (!result.res)
	    fprintf(stderr, "Invalid value for \'%s\': %s\n", optText, ArgVal);
	break;
    case OPT_DEVADR:
	result.key = "address";
	result.val = QString(ArgVal).toUInt(&result.res);
	if ((!result.res) || (result.val.toUInt() > 254)
		|| (result.val.toUInt() == 0))
	    {
	    fprintf(stderr, "Invalid value for \'%s\': %s, mast be [1..254].\n",
		    optText, ArgVal);
	    result.res = false;
	    }
	break;
    case OPT_OUT:
	{
	QString o(ArgVal);
	result.key = "Output";
	if ((o.at(o.length() - 1) == '/')
#ifdef WIN32
	|| (o.at(o.length() - 1) == '\\')
#endif
	)
	    {
#ifdef WIN32
	    o.replace('\\', '/');
#endif
	    result.val = o + (*origins)["Output"].toString();
	    }
	else
	    result.val = ArgVal;
	}
	break;
    case OPT_APPEND:
	result.key = "OpenMode";
	result.val = (uint) (QIODevice::WriteOnly | QIODevice::Append);
	break;
    case OPT_AUTO:
	result.key = "address";
	result.val = DeviceAdress_All_Found;
	break;
    case OPT_NOFORCE:
	result.key = "Force";
	result.val = false;
	break;
    case OPT_FROM:
	result.key = "Start";
	result.val = QString(ArgVal).toUInt(&result.res);
	if ((!result.res) || (result.val.toUInt() > 65534))
	    {
	    fprintf(stderr,
		    "Invalid value for \'%s\': %s, mast be [0..65534].\n",
		    optText, ArgVal);
	    result.res = false;
	    }
	break;
    case OPT_TO:
	{
	result.key = "End";
	result.val = QString(ArgVal).toUInt(&result.res);
	if ((!result.res) || (result.val.toUInt() > 65535))
	    {
	    fprintf(stderr,
		    "Invalid value for \'%s\': %s, mast be [0..65535].\n",
		    optText, ArgVal);
	    result.res = false;
	    }
	}
	break;
    case OPT_LOG_LEVEL:
	{
	QString str(ArgVal);
	result.key = "Log/Log-level";
	if (str == QObject::trUtf8("Debug"))
	    result.val = MyLog::LOG_DEBUG;
	else if (str == QObject::trUtf8("Info"))
	    result.val = MyLog::LOG_INFO;
	else if (str == QObject::trUtf8("Warning"))
	    result.val = MyLog::LOG_WARNING;
	else if (str == QObject::trUtf8("Error"))
	    result.val = MyLog::LOG_ERROR;
	else
	    {
	    int res = str.toUInt(&result.res);
	    if ((result.res) && ((MyLog::enLogLevel) res >= MyLog::LOG_FATAL)
		    && ((MyLog::enLogLevel) res <= MyLog::LOG_DEBUG))
		result.val = res;
	    else
		{
		fprintf(
			stderr,
			"Invalid value for \'%s\': %s, mast be [Error|Debug|Info|Warning].\n",
			optText, ArgVal);
		result.res = false;
		}
	    }
	}
	break;
    case OPT_READ_AT_ONCE:
	result.key = "ReadAtOnce";
	result.val = QString(ArgVal).toUInt(&result.res);
	if ((!result.res) || (result.val.toUInt() > 120)
		|| (result.val.toUInt() < 1))
	    {
	    fprintf(stderr, "Invalid value for \'%s\': %s, mast be [1..120].\n",
		    optText, ArgVal);
	    result.res = false;
	    }
	break;
    case OPT_COEFFS_ONLY:
	result.key = "SaveOpts";
	result.val = SaveOpts_Coeffs_save;
	break;
    case OPT_NO_COEFFS:
	result.key = "SaveOpts";
	result.val = SaveOpts_Data_save;
	break;
    case OPT_TEST_ONLY:
	result.key = "SaveOpts";
	result.val = SaveOpts_TestOnly
		| ((*origins)["SaveOpts"].toUInt() & SaveOpts_Clear_device);
	break;
    case OPT_TEST_CLEAR:
	result.key = "TestClear";
	result.val = true;
	break;
    case OPT_CLEAR_DEVICE:
	result.key = "SaveOpts";
	result.val = SaveOpts_Clear_device
		| ((*origins)["SaveOpts"].toUInt() & SaveOpts_TestOnly);
	break;
    default:
	result.res = false;
	break;
	}
    return result;
    }

void CmdLineAnalyser::SetDefaultValues(cSettingsCmdLine* _this)
    {
    _this->insert("Global/Port", _DEFAULT_PORT);
    _this->insert("Global/Baud", _DEFAULT_SPEED);
    _this->insert("address", DeviceAdress_First_Found);
    _this->insert("Output", "%serial%_%date%.ARP.dump");
    _this->insert("Start", 0);
    _this->insert("End", -1); // до счетчика
    _this->insert("Log/Log-level", MyLog::LOG_INFO);
    _this->insert("Force", true);
    _this->insert("ReadAtOnce", ARP_MAX_READ_ONCE);
    _this->insert("OpenMode", QIODevice::WriteOnly); // переписывать файл данных
    _this->insert("SaveOpts", SaveOpts_Coeffs_save | SaveOpts_Data_save); // сохранять и коэфициенты и данные
    _this->insert("TestClear", false); // проверка на очищенность
    }

CmdLineAnalyser::CmdLineAnalyser(int argc, char *argv[]) :
	cSettingsCmdLine(argc, argv, "", &table)
    {
    }
