#include "CleanExit.h"

static CleanExit *pCleanExit;

CleanExit::CleanExit(QObject *parent) :
	QObject(parent)
    {
    pCleanExit = this;
#ifdef WIN32
    signal(SIGINT, &CleanExit::exitQt);
    signal(SIGTERM, &(CleanExit::exitQt));
#endif
    }

void CleanExit::exitQt(int sig)
    {
    qDebug() << "Shutdown application CTRL+C.";
    qApp->quit();
    }
