//-----------------------------------------------------------------
#include <QCoreApplication>
#include <QString>
#include <QList>
#include <QTimer>
#include <QTranslator>
//----------------------------------------------------------------
#include <mylog/mylog.h>
#include <qmodbus/common.h>
#include <qmodbus/ModbusEngine.h>
//----------------------------------------------------------------
#ifdef WIN32
#include <windows.h>
#include <clocale>
#endif
//-----------------------------------------------------------------
#include "CleanExit.h"
#include "CmdLineAnalyser.h"
#include "ARP.h"
#include "ARPReader.h"
//-----------------------------------------------------------------
#include "Revision.h"

int main(int argc, char *argv[])
    {
    QCoreApplication app(argc, argv);
    app.setApplicationName("ARPFlashDumper");
    app.setApplicationVersion(GetRevisionNumber());

#ifdef _USE_TRANSLATION
#ifdef WIN32
    OSVERSIONINFO osvi;
    memset(&osvi, 0, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    GetVersionEx(&osvi);
    if (osvi.dwPlatformId == VER_PLATFORM_WIN32_NT && osvi.dwMajorVersion >= 6)
	{
	setlocale(LC_ALL, "Russian");
	QTranslator myappTranslator;
	myappTranslator.load("tr_" + QLocale::system().name(), "res");
	app.installTranslator(&myappTranslator);
	}
#else
    QTranslator myappTranslator;
    myappTranslator.load("tr_" + QLocale::system().name(), "res");
    app.installTranslator(&myappTranslator);
#endif
#endif

    CmdLineAnalyser _settings(argc, argv);

    if (!SettingsCmdLine::settings->isOk())
	return 1;

    //Лог
    MyLog::myLog _LOG;

    // синхронно-асинхронный движок Modbus
    ModbusEngine _ModbusCore;

    // обработчик закрытия программы по CTRL+C
    CleanExit cleanExit;

    QList<ARP*> arpList;

    switch ((*SettingsCmdLine::settings)["address"].toUInt())
	{
    case CmdLineAnalyser::DeviceAdress_First_Found:
	{
	LOG_WARNING("Detecting device, please wait.");
	ARP* dev = ARP::detector(1, &_ModbusCore);
	if (dev != NULL)
	    {
	    LOG_INFO(
		    QObject::trUtf8("ARP device detected at address %1, serial number: %2").arg(dev->getadress(),2,16).arg(dev->getSerial()));
	    arpList.append(dev);
	    }
	else
	    {
	    LOG_ERROR(QObject::trUtf8("Detecting failed!"));
	    }
	}
	break;
    case CmdLineAnalyser::DeviceAdress_All_Found:
	{
	LOG_WARNING(QObject::trUtf8("Detecting devices, please wait."));
	unsigned char lastfound = 1;
	ARP* dev;
	while ((dev = ARP::detector(lastfound, &_ModbusCore)) != NULL)
	    {
	    lastfound = dev->getadress();
	    LOG_INFO(
		    QObject::trUtf8("ARP device detected at address %1, serial number: %2").arg(lastfound++,2,16).arg(dev->getSerial()));
	    arpList.append(dev);
	    } //
	LOG_INFO(
		QObject::trUtf8("Detection finished: %1.").arg(arpList.empty() ? QObject::trUtf8("Fail"): QObject::trUtf8("Success")));
	}
	break;
    default:
	{
	ARP *arp = new ARP((*SettingsCmdLine::settings)["address"].toUInt(),
		&_ModbusCore);
	if (arp->test())
	    { // тест ок
	    LOG_INFO(
		    QObject::trUtf8("ARP device detected at address %1, serial number: %2").arg(arp->getadress(),2,16).arg(arp->getSerial()));
	    arpList.append(arp);
	    }
	else
	    {
	    LOG_ERROR(
		    QObject::trUtf8("Specified ARP device not detected at address %1. Exit.").arg(arp->getadress(),2,16));
	    delete arp;
	    return 3;
	    }
	}
	break;
	}

    if (arpList.isEmpty())
	{
	LOG_ERROR(QObject::trUtf8("No ARP devices found! Exiting."));
	return 5;
	}

    ARPReader reader(&arpList);

    QTimer::singleShot(100, &reader, SLOT(Exec()));
    int res = app.exec();

    // удаляем созданные устройства
    foreach(ARP* _arp, arpList)
	delete _arp;

    return res;
    }
//-----------------------------------------------------------------
