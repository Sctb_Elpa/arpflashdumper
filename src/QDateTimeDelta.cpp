/*
 * QDateTimeDelta.cpp
 *
 *  Created on: 05.02.2013
 *      Author: tolyan
 */

#include <QTime>

#include "QDateTimeDelta.h"

#define SEC_MS	1000
#define MIN_MS	(SEC_MS * 60)
#define HOUR_MS	(MIN_MS * 60)
#define DAY_MS	(HOUR_MS * 24)

QString QDateTimeDelta(const QDateTime & t1, const QDateTime & t2)
    {
    int diff_days = t2.daysTo(t1);
    int64_t diff_ms = (int64_t)(t2.time().msecsTo(t1.time())) %
	    ((int64_t)DAY_MS);
    if (diff_ms < 0)
	{
	--diff_days;
	diff_ms += DAY_MS;
	}
    QTime time(
	    QTime(diff_ms / HOUR_MS, (diff_ms % HOUR_MS) / MIN_MS,
		    (diff_ms % MIN_MS) / SEC_MS, diff_ms % SEC_MS));
    return QObject::trUtf8("%1 %2").arg(diff_days).arg(time.toString());
    }
