/*
 * ARPReader.cpp
 *
 *  Created on: 05.02.2013
 *      Author: tolyan
 */

#include <cmath>
#include <iostream>
#include <stdint.h>
#include <cstdlib>
#include <cstring>

#include <QCryptographicHash>
#include <QCoreApplication>
#include <QString>
#include <QDateTime>
#include <QFile>

#include <qjson/serializer.h>
#include <mylog/mylog.h>
#include <modbus/modbus-rtu.h>

#include "ARP.h"
#include "CmdLineAnalyser.h"
#include "QDateTimeDelta.h"

#include "ARPReader.h"

#define DISABLE_OUTPUT 	(0)
#define TESTDUMP_OUT	(0)

QByteArray ARPReader::md5CorrectHash;

char ARPReader::m[] =
	{
		'-', '\\', '|', '/'
	};

ARPReader::ARPReader(QList<ARP*>* _arpList, QObject *parent) :
				QObject(parent)
    {
    arpList = _arpList;
    pos = 0;
    outputFreqDev = 0;
    if (md5CorrectHash.isEmpty())
	{
	uint16_t buffer[ARP_BLOCK_SIZE];
	if ((*SettingsCmdLine::settings)["TestClear"].toBool())
	    {
	    std::memset(buffer, 0xff, ARP_BLOCK_SIZE * 2);
	    }
	else
	    {
	    buffer[0] = 0xA5A5;
	    for (int i = 1; i < ARP_BLOCK_SIZE; ++i)
		{
		buffer[i] = crc16((uint8_t*) &buffer[i - 1], sizeof(uint16_t));
		buffer[i] += buffer[i - 1];
		}
	    }
#if TESTDUMP_OUT == 1
	QFile testdump("test.dump");
	testdump.open(QIODevice::WriteOnly);
	testdump.write((char*) buffer, ARP_BLOCK_SIZE * sizeof(uint16_t));
	testdump.close();
#endif
	md5CorrectHash = QCryptographicHash::hash(
		QByteArray::fromRawData((char*) buffer, sizeof(buffer)),
		QCryptographicHash::Md5);
	}
    }

ARPReader::~ARPReader()
    {
    }

void ARPReader::Exec()
    {
    bool fForce = (*SettingsCmdLine::settings)["Force"].toBool();
    QList<ARP*>::iterator it = arpList->begin();
    for (; it < arpList->end(); ++it)
	{
	bool r1, r2;
	uint16_t serial = (*it)->getSerial(&r1);
	uint32_t memcounter = (*it)->getMemoryCounter(&r2);
	if ((r1 && r2) || (fForce))
	    {
	    time = QDateTime::currentDateTime();
	    if ((*SettingsCmdLine::settings)["SaveOpts"].toUInt()
		    != CmdLineAnalyser::SaveOpts_Clear_device)
		{ // __НЕ__ только стирание
		connect(*it, SIGNAL(UpdateProgress(qreal)), this,
			SLOT(ShowProgress(qreal)));
		LOG_INFO(
			trUtf8("Start reading device %1, Serial: %2, memory used: %3 Bytes").arg((*it)->getadress(), 2,16).arg(serial).arg(memcounter));
		LOG_INFO(trUtf8("Current device date: %1").arg((*it)->getDate().toString(Qt::ISODate)));
		QString filename =
			(*SettingsCmdLine::settings)["Output"].toString();
		if (filename.contains("%serial%"))
		    {
		    filename.replace("%serial%",
			    "S-" + QString::number(serial));
		    }
		if (filename.contains("%date%"))
		    {
		    QDate date = QDate::currentDate();
		    filename.replace("%date%", date.toString(Qt::ISODate));
		    } //
		//--coefs--
		if ((*SettingsCmdLine::settings)["SaveOpts"].toUInt()
			& CmdLineAnalyser::SaveOpts_Coeffs_save)
		    {
		    // сохранение коэфициентов, если нужно
		    QString coefsFileName = filename;
		    if (coefsFileName.contains(".dump")
			    && coefsFileName.at(coefsFileName.length() - 1)
			    == 'p')
			// если имя файла данных оканчивается на '.dump', заменяем его на '.coeffs'
			coefsFileName.replace(".dump", ".coeffs");
		    else
			// иначе, дописываем '.coeffs' в конец
			coefsFileName += ".coeffs";
		    LOG_INFO(trUtf8("Coeffs file name: %1").arg(coefsFileName));
		    QFile coeffsFile(coefsFileName);
		    if (!coeffsFile.open(QIODevice::WriteOnly))
			{
			if (fForce)
			    {
			    LOG_ERROR(
				    trUtf8("Failed to open coeffs file! Skipping."));
			    }
			else
			    {
			    LOG_FATAL(
				    trUtf8("Failed to open coeffs file! Exiting."));
			    qApp->quit();
			    return;
			    }
			}
		    else
			{
			QJson::Serializer serializer;
			bool okTest;
			QVariantMap coeffsDataMap = (*it)->getCoeffs(&okTest);
			if (okTest)
			    {
			    if (MyLog::LOG->getLogLevel() == MyLog::LOG_DEBUG)
				{ // в дебаге выводим коэфициенты на экран
				LOG_DEBUG(trUtf8("Coeffs:"));
				foreach(QString key, coeffsDataMap.keys())
				    {
				    LOG_DEBUG(
					    trUtf8("\t\"%1\": %2").arg(key).arg(coeffsDataMap[key].toFloat()));
				    }
				}
			    serializer.serialize(coeffsDataMap, &coeffsFile,
				    &okTest);
			    if (!okTest)
				{
				if (fForce)
				    LOG_ERROR(
					    trUtf8("Failed to save coeffs to file, skipping."));
				else
				    {
				    LOG_FATAL(
					    trUtf8("Failed to save coeffs to file, exiting"));
				    qApp->quit();
				    return;
				    }
				}
			    }
			coeffsFile.close();
			}
		    }
		//--coefs--

		//--data--
		if ((*SettingsCmdLine::settings)["SaveOpts"].toUInt()
			!= CmdLineAnalyser::SaveOpts_Coeffs_save)
		    {
		    // сохранение данных
		    QFile outfile(filename);
		    if ((*SettingsCmdLine::settings)["SaveOpts"].toUInt()
			    & CmdLineAnalyser::SaveOpts_TestOnly)
			LOG_INFO(
				trUtf8("%1 started. Correct MD5 hash: %2").arg(
					(*SettingsCmdLine::settings)["TestClear"].toBool() ?
						"Clear test" : "Test").arg(
							QString(md5CorrectHash.toHex())));
		    else
			{
			LOG_INFO(
				trUtf8("Output file: %1 (mode: %2).").arg(filename).arg(((QIODevice::OpenMode) (*SettingsCmdLine::settings)["OpenMode"].toUInt() == QIODevice::WriteOnly) ? trUtf8("rewrite"): trUtf8("append")));

			if (!outfile.open(
				(QIODevice::OpenMode) (*SettingsCmdLine::settings)["OpenMode"].toUInt()))
			    {
			    LOG_FATAL(
				    trUtf8("Failed to open data file! Exiting."));
			    qApp->quit();
			    return;
			    }
			}

		    int CurrentBlock =
			    (*SettingsCmdLine::settings)["Start"].toUInt();
		    int EndBlock = (*SettingsCmdLine::settings)["End"].toUInt();
		    if (EndBlock == -1)
			EndBlock = ceil(
				(qreal) memcounter / (2 * ARP_BLOCK_SIZE)) - 1; //округляем вверх и -1, ибо с нуля
		    if (EndBlock == -1)
			{
			if ((*SettingsCmdLine::settings)["TestClear"].toBool())
			    EndBlock = 65535;
			else
			    {
			    LOG_FATAL(trUtf8("Device is empty!"));
			    qApp->quit();
			    return;
			    }
			}
		    if (EndBlock < CurrentBlock)
			{
			LOG_ERROR(
				trUtf8("'From to Error' (%1 > %2). Break.").arg(CurrentBlock).arg(EndBlock));
			continue;
			} //
		    LOG_INFO(
			    trUtf8("Start block: %1\tEnd block: %2").arg(CurrentBlock).arg(EndBlock));

		    offset = CurrentBlock;
		    endblock = EndBlock;
		    QByteArray res;
		    do
			{
			currentPos = CurrentBlock;
			MyLog::LOG->newLineRequest();
			res = (*it)->dataAtBlock(CurrentBlock, &r1);
			if (!r1)
			    {
			    MyLog::LOG->newLineRequest();
			    LOG_ERROR(
				    trUtf8("Error while reading block %1.").arg(CurrentBlock));
			    if (!fForce)
				{
				LOG_ERROR(
					trUtf8("Error While processing device %1!, break. \n (Note: specify '--force' flag to ignore this error).").arg((*it)->getadress(), 2, 16));
				break;
				}
			    } //
			if ((*SettingsCmdLine::settings)["SaveOpts"].toUInt()
				& CmdLineAnalyser::SaveOpts_TestOnly)
			    {
			    QByteArray currentHash = QCryptographicHash::hash(
				    res, QCryptographicHash::Md5);
			    if (currentHash != md5CorrectHash)
				{
				MyLog::LOG->newLineRequest();
				LOG_ERROR(
					trUtf8("Block %1 have incorrect test MD5 summ: %2").arg(CurrentBlock).arg(QString(currentHash.toHex())));
				}
			    }
			else
			    {
			    MyLog::LOG->newLineRequest();
			    LOG_DEBUG(
				    trUtf8("Block %1 MD5: %2").arg(CurrentBlock).arg(QString(QCryptographicHash::hash(res, QCryptographicHash::Md5).toHex())));

			    outfile.write(res); // запись
			    }
			QCoreApplication::processEvents();
			}
		    while (++CurrentBlock <= EndBlock);
		    if ((*SettingsCmdLine::settings)["SaveOpts"].toUInt()
			    & CmdLineAnalyser::SaveOpts_Data_save)
			outfile.close();
		    disconnect(*it, SIGNAL(UpdateProgress(qreal)), this,
			    SLOT(ShowProgress(qreal)));
		    std::cout << std::endl; // новая строка
		    if (!((*SettingsCmdLine::settings)["SaveOpts"].toUInt()
			    & CmdLineAnalyser::SaveOpts_Clear_device))
			{
			LOG_INFO(
				trUtf8("Complied! Elapsed time: %1").arg(QDateTimeDelta(QDateTime::currentDateTime(), time)));
			continue; // __НЕ__ __И__ стирать.
			}
		    }
		}
	    // стирание
	    if ((*SettingsCmdLine::settings)["SaveOpts"].toUInt()
		    & CmdLineAnalyser::SaveOpts_Clear_device)
		{
		LOG_INFO(trUtf8("Clearing device..."));
		(*it)->Erease();
		}
	    }
	else
	    {
	    LOG_ERROR(trUtf8("Error While processing device info!"));
	    }
	} //
    LOG_INFO(trUtf8("Complied!"));
    qApp->quit();
    }

void ARPReader::ShowProgress(qreal pAdd)
    {
#if (DISABLE_OUTPUT!=1)
    if (!(outputFreqDev = outputFreqDev++ % 5)) // выводить строку по-реже
	{
	int devider = endblock - offset + 1;
	qreal progress = ((qreal) (currentPos - offset) + pAdd) / devider * 100;
	char krutilka = m[pos = (pos++ % 4)];
	printf(
		"\rReading block: %i/%i\t%c\t%.2f%%\t%s",
		currentPos,
		endblock,
		krutilka,
		progress,
		QDateTimeDelta(QDateTime::currentDateTime(), time).toLatin1().data());
	fflush(stdout);
	}
#endif
    }

