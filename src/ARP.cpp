/*
 * ARP.cpp
 *
 *  Created on: 04.02.2013
 *      Author: tolyan
 */

#include <cstdio>
#include <cstdlib>

#include <QStringList>
#include <QCoreApplication>

#include <mylog/mylog.h>

#include <qmodbus/ModbusRequest.h>

#include <qmodbus/ModbusEngine.h>
#include <qmodbus/common.h>
#include <qmodbus/Sleeper.h>

#include "ARP.h"

unsigned int ARP::readAtOnce;

static char tableOfProcessMarkers[] =
    {
    '-', '\\', '|', '/'
    };

static int8_t BCDToByte(int8_t source)
{
    return (int8_t)(((source & 0xF0) >> 4) * 10 + (source & 0x0F));
}

ARP::ARP(unsigned char mb_adress, ModbusEngine* _mb, QObject *parent) :
	QObject(parent)
    {
    mb = _mb;
    mbadress = mb_adress;
    readAtOnce = (*SettingsCmdLine::settings)["ReadAtOnce"].toUInt();
    }

ARP::~ARP()
    {
    if (!readRequests.empty())
	foreach(ModbusRequest* req, readRequests)
	    req->deleteLater();
    }

void ARP::buildreadRequests()
    {
    uint16_t currentAdress = 0x0000;
    ModbusRequest* req;
    while (currentAdress < ARP_BLOCK_SIZE)
	{
	int toRead = readAtOnce;
	if ((currentAdress + toRead) > ARP_BLOCK_SIZE)
	    toRead = ARP_BLOCK_SIZE - currentAdress;
	req = ModbusRequest::BuildReadInputsRequest(mbadress,
		currentAdress + ARP_DATA_START_ADDRESS, toRead);
	readRequests.append(req);
	currentAdress += toRead;
	}
    }

QVariantMap ARP::getCoeffs(bool *ok)
    {
    bool _ok = true;
    QVariantMap result;
    ModbusRequest* coeffsRequest = ModbusRequest::BuildReadHoldingRequest(
	    mbadress, ARP_COEFFS_START_ADRESS, ARP_COEFFS_COUNT * 2);
    mb->SyncRequest(*coeffsRequest);
    if (coeffsRequest->getErrorrCode() != ModbusRequest::ERR_OK)
	{
	LOG_ERROR(trUtf8("Error while reading coeffs."));
	_ok = (*SettingsCmdLine::settings)["Force"].toBool();
	}
    else
	{
	QStringList koeffsNames; // имена коэфициентов
	koeffsNames << "A0" << "A1" << "A2" << "A3" << "A4" << "A5" << "Ft0"
		<< "Fp0" << "T0" << "C1" << "C2" << "C3" << "F0";
	QList<float> coeffsVals = coeffsRequest->getAnsverAsFloat();
	for (int i = 0; i < koeffsNames.size(); ++i)
	    result.insert(koeffsNames.at(i), QVariant(coeffsVals.at(i)));
	}
    delete coeffsRequest;
    if (ok)
	*ok = _ok;
    return result;
    }

bool ARP::writeBlockAdress(uint16_t block)
    {
    bool res = true;
    ModbusRequest* writeAddressReq = ModbusRequest::BuildWriteHoldingRequest(
	    mbadress, ARP_BLOCK_SELECTOR_ADDRESS,
	    QByteArray::fromRawData((char*) &block, sizeof(uint16_t)));
    while (true)
	{
	mb->SyncRequest(*writeAddressReq);
	if (writeAddressReq->getErrorrCode() != ModbusRequest::ERR_OK)
	    {
	    if (SettingsCmdLine::settings->value("Force", false).toBool())
		LOG_DEBUG(
			trUtf8("Error writing address selector value, repeat."));
	    else
		{
		LOG_ERROR(trUtf8("Error writing address selector value."));
		res = false;
		break;
		}
	    }
	else
	    break;
	}
    delete writeAddressReq;
    return res;
    }

QByteArray ARP::dataAtBlock(uint16_t block, bool *ok)
    {
    QByteArray res;
    QByteArray hash;
    if (readRequests.isEmpty())
	buildreadRequests();
    // write address
    if (!writeBlockAdress(block))
	{
	if (ok)
	    *ok = false;
	return QByteArray(ARP_BLOCK_SIZE * 2, 0xff);
	}
    //read

    QList<ModbusRequest*>::iterator it = readRequests.begin();
    QList<ModbusRequest*>::iterator end = readRequests.end();
    int counter = 1;
    bool isOk = true;
    for (; it < end; ++it)
	{
#ifndef INF_REREAD_TRY
	if ((*SettingsCmdLine::settings)["Force"].toBool())
	    {
#endif
	    while (true)
		{
		if (mb->SyncRequest(**it) != 0)
		    { //!0 при совсем фэйле, останавливаемся
#ifndef _NODEVICES
		    isOk = false;
#endif
		    break;
		    }
		else
		    { // проверим код ошибки
		    if ((*it)->getErrorrCode() != ModbusRequest::ERR_OK)
			{
			if (!((*SettingsCmdLine::settings)["Force"].toBool()))
			    {
			    LOG_WARNING(
				    trUtf8("Error %1 occurred while reading, rereading fragment").arg((*it)->getErrorrCode()));
			    MyLog::LOG->newLineRequest();
			    }
			else
			    {
			    LOG_DEBUG(
				    trUtf8("Error %1 occurred while reading, rereading fragment").arg((*it)->getErrorrCode()));
			    MyLog::LOG->newLineRequest();
			    }
			Sleeper::msleep(50); //вслучае её возникновения ждем и пробуем заново
			}
		    else
			{
			break; //все ок, продолжаем.
			}
		    }
		}
#ifndef INF_REREAD_TRY
	    }
	else
	    {
	    if (mb->SyncRequest(**it) != 0)
		{ //!0 при совсем фэйле, останавливаемся
#ifndef _NODEVICES
		isOk = false;
#endif
		}
	    else
		{
		// проверим код ошибки
		if ((*it)->getErrorrCode() != ModbusRequest::ERR_OK)
		    {
		    LOG_ERROR(
			    trUtf8("Error %1 occurred while reading, fragment is now filled '0xff'").arg((*it)->getErrorrCode()));
		    (*it)->setAnsver(
			    QByteArray(
				    ((it == end - 1) ?
					    (ARP_BLOCK_SIZE % readAtOnce) :
					    (readAtOnce)) * 2, 0xff));
		    }
		}
	    }
#endif
	if (!isOk)
	    {
	    LOG_ERROR(
		    trUtf8("General modbus error occurred while reading, block is now filled '0xff'"));
	    if (ok)
		*ok = false;
	    return QByteArray(ARP_BLOCK_SIZE * 2, 0xff);
	    } //
	res.append((*it)->getAnsver());
	emit
	UpdateProgress((qreal) counter / readRequests.size());
	++counter;
	}
    if (ok)
	*ok = true;
    return res;
    }

uint16_t ARP::getSerial(bool* isOk)
    {
    bool ok;
    ModbusRequest* serialReq = ModbusRequest::BuildReadHoldingRequest(mbadress,
	    ARP_SERIAL_ADDRESS, 1);
    mb->SyncRequest(*serialReq);
    uint16_t res = 0xffff;
    if (serialReq->getErrorrCode() == ModbusRequest::ERR_OK)
	{
	res = serialReq->getAnsverAsShort().at(0);
	ok = true;
	}
    else
	{
	LOG_ERROR(
		trUtf8("Failed to read serial number from device at address %1.").arg(mbadress, 2,16));
	ok = false;
	}
    delete serialReq;
    if (isOk)
	*isOk = ok;
    return res;
    }

bool ARP::test()
    {
    bool res;
    ModbusRequest* testReq = ModbusRequest::BuildReadHoldingRequest(mbadress,
	    ARP_DEVICE_ID_ADDRESS, 1);
    mb->SyncRequest(*testReq);
    if (testReq->getErrorrCode() != ModbusRequest::ERR_OK)
	res = false;
    else
	{
	uint16_t id = testReq->getAnsverAsShort().at(0);
	if (id == ARP_DEVICE_ID)
	    res = true;
	else
	    {
	    LOG_WARNING(
		    trUtf8("Found unsupported device ID: %1 at address %2").arg(id, 4, 16).arg(mbadress, 2, 16));
	    res = true;
	    }
	}
    delete testReq;
    return res;
    }

unsigned char ARP::getadress() const
    {
    return mbadress;
    }

uint32_t ARP::getMemoryCounter(bool* isOk)
    {
    bool ok;
    ModbusRequest* memCounterReq = ModbusRequest::BuildReadInputsRequest(
	    mbadress, ARP_MEMORY_COUNTER_ADDRESS, 2);
    mb->SyncRequest(*memCounterReq);
    uint32_t res = 0xffffffff;
    MyLog::LOG->newLineRequest();
    if (memCounterReq->getErrorrCode() == ModbusRequest::ERR_OK)
	{
	res = memCounterReq->getAnsverAsShort().at(0)
		+ ((uint32_t) memCounterReq->getAnsverAsShort().at(1) << 16);
	LOG_DEBUG(
		trUtf8("Device at address %1 memory used: %2 bytes.").arg(mbadress, 2,16).arg(res));
	ok = true;
	}
    else
	{
	LOG_ERROR(
		trUtf8("Failed to read memory counter from device at address %1.").arg(mbadress, 2,16));
	ok = false;
	}
    delete memCounterReq;
    if (isOk)
	*isOk = ok;
    return res;
    }

ARP* ARP::detector(unsigned char startAdress, ModbusEngine* mb)
    {
    ARP* testdev = NULL;
    while (startAdress < 255)
	{
	/*
	 printf("\rScaning address: %02X", startAdress);
	 fflush(stdout);
	 */
	testdev = new ARP(startAdress++, mb);
	if (testdev->test())
	    break; //ok
	else
	    {
	    delete testdev; //!ok
	    testdev = NULL;
	    }
	}
    printf("\n");
    return testdev;
    }

bool ARP::Erease()
    {
    // для стирания нужно зписать 1 в коил CLEAR_FLAG_ADDRES
    ModbusRequest* clearRequest = ModbusRequest::BuildWriteSingleCoilRequest(
	    mbadress, CLEAR_FLAG_ADDRES, true);
    mb->SyncRequest(*clearRequest);
    if (clearRequest->getErrorrCode() != ModbusRequest::ERR_OK)
	{
	LOG_ERROR(trUtf8("Failed to clear device (request failed)."));
	return false;
	}
    else
	{
	bool ok;
	uint32_t memoryUsed = getMemoryCounter(&ok);
	if (memoryUsed == 0)
	    {
	    LOG_WARNING(trUtf8("Device is empty, nothing to do."));
	    return true;
	    }
	uint32_t currentMemoryUsed = memoryUsed;
	float present;
	char i = 0;
	if (!ok)
	    return false; // это ошибка вобще-то
	while (true)
	    {
	    currentMemoryUsed = getMemoryCounter(&ok);
	    present = (memoryUsed - currentMemoryUsed) / (float) memoryUsed
		    * 100;
	    if (ok)
		{
		printf(
			"\rClearing:\t%c\t%.2f%%",
			tableOfProcessMarkers[i = ++i
				% sizeof(tableOfProcessMarkers)], present);
		fflush(stdout);
		if (currentMemoryUsed == 0)
		    return true;
		QCoreApplication::processEvents();
		Sleeper::sleep_ms(500);
		}
	    }
	}
    }

QDateTime ARP::getDate(bool *ok)
    {
    if (ok != NULL)
	*ok = false;
    ModbusRequest* getDateReq = ModbusRequest::BuildReadHoldingRequest(mbadress,
	    ARP_DATE_START_ADRESS, 3);
    struct timeval oldTimeout;
    modbus_get_response_timeout(mb->getModbusContext(), &oldTimeout);
    struct timeval LongTimeout =
            {
                0, 200000
            };
    modbus_set_response_timeout(mb->getModbusContext(), &LongTimeout);
    mb->SyncRequest(*getDateReq);
    modbus_set_response_timeout(mb->getModbusContext(), &oldTimeout);
    if (getDateReq->getErrorrCode() != ModbusRequest::ERR_OK)
	{
	LOG_ERROR(
		trUtf8("Failed to get date from device. (%1)").arg(getDateReq->getErrorrCode()));
	}
    else
	{
	QByteArray res = getDateReq->getAnsver();
	if (res.length() != 6)
	    {
	    LOG_ERROR(trUtf8("Incorrect answer for getDate request."));
	    }
	else
	    {
	    union U_Date rawDate;
	    memcpy(&rawDate, res.data(), sizeof(U_Date));
	    for(int i = 0; i < sizeof(U_Date); ++i)
		rawDate.raw[i] = BCDToByte(rawDate.raw[i]);
	    QDateTime result(
		    QDate(rawDate._Date.year + 2000, rawDate._Date.month, rawDate._Date.day),
		    QTime(rawDate._Date.hour, rawDate._Date.min, rawDate._Date.sec));
	    if (!result.isValid())
		{
		char datestr[100];
		sprintf(datestr, "%i.%i.%i %i:%i:%i", rawDate._Date.year + 2000,
			rawDate._Date.month, rawDate._Date.day, rawDate._Date.hour, rawDate._Date.min,
			rawDate._Date.sec);
		LOG_ERROR(trUtf8("Incorrect date: %1").arg(datestr));
		}
	    else if (ok != NULL)
		*ok = true;
	    __DELETE(getDateReq);
	    return result;
	    }
	}
    __DELETE(getDateReq);
    return QDateTime::currentDateTime();
    }
